#include "mkrecv_rnt.h"

int main(int argc, const char **argv)
{
  mkrecv::receiver  recv;

  return recv.execute(argc, argv);
}

/*
  ./mkrecv_rnt --header header.cfg --threads 16 \
  --heaps 64 \
  --dada 4 \
  --freq_first 0 --freq_step 256 --freq_count 4 \
  --feng_first 0 --feng_count 4 \
  --time_step 2097152 \
  --port 7148 \
  --udp_if 10.100.207.50 \
  --packet 1500 \
  239.2.1.150 239.2.1.151 239.2.1.152 239.2.1.153


*/
