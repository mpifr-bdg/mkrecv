#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <endian.h>
#include <math.h>

#include "fftw3.h"

#define SAMPLES_WAVE   0
#define SINE_WAVE      1
#define SQUARE_WAVE    2

static int idxSample = 0;

/**
   @brief Rounds a floating point value to a given relative precision.
   @param x The floating point value which will be rounded.
   @param precision The final relative precision of the value.
   @returns The rounded value.

   This function rounds a floating point value to a specific precision. 
   The precision parameter specifies the number of significant digits.
*/
static double mat_round_relative(double x, int precision)
{
  int  l;
  int neg = 0;

  if (x == 0.0) return 0.0;
  if (x < 0.0)
    {
        neg = 1;
        x = -x;
    }
  l = (int)ceil(log10(x));
  if (l > 0)
    {
        if (l > precision)
        { // more digits are before the decimal point than the precision
            // -> divide - round - multiply
            double scale = pow(10.0, (double)(l - precision));
            x /= scale;
            x = round(x);
            x *= scale;
        }
        else
        {
            double scale = pow(10.0, (double)(precision - l));
            x *= scale;
            x = round(x);
            x /= scale;
        }
    }
  else
    {
        double scale = pow(10.0, (double)(precision - l));
        x *= scale;
        x = round(x);
        x /= scale;
    }
  if (neg)
    {
        return -x;
    }
  else
    {
        return x;
    }
}

static int read_samples_double(double *dest, uint64_t *buffer, int nbits, int nsamples, int *ndump, int waveform, double sample_freq, double wave_freq, FILE *data)
{
  static double wave_phi    = 0.0;
  static double wave_length = sample_freq/wave_freq;
  size_t    nwords = nsamples*nbits/64;
  uint64_t  val64, rest64;
  size_t    iWord   = 0;
  int       iSample = 0;
  double   *odest = dest;
  size_t    words_read;
  int       round_flag = (waveform < 0);

  if (waveform < 0) waveform = -waveform;
  if (data != NULL) {
    if (feof(data)) return 0;
    words_read = fread(buffer, sizeof(uint64_t), nwords, data);
    if (words_read != nwords) return 0;
    // decode samples into complex numbers (from udprec.c)
    if (nbits == 8) {
      while (iWord < nwords) {
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)((0xFF00000000000000 & val64) <<  0) >> 56);
	*dest++ = (double)((int64_t)((0x00FF000000000000 & val64) <<  8) >> 56);
	*dest++ = (double)((int64_t)((0x0000FF0000000000 & val64) << 16) >> 56);
	*dest++ = (double)((int64_t)((0x000000FF00000000 & val64) << 24) >> 56);
	*dest++ = (double)((int64_t)((0x00000000FF000000 & val64) << 32) >> 56);
	*dest++ = (double)((int64_t)((0x0000000000FF0000 & val64) << 40) >> 56);
	*dest++ = (double)((int64_t)((0x000000000000FF00 & val64) << 48) >> 56);
	*dest++ = (double)((int64_t)((0x00000000000000FF & val64) << 56) >> 56);
      }
    } else if (nbits == 10) {
      while (iWord < nwords) {
	// 1st: 64-bit word -> 6 samples and 4 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(( 0xFFC0000000000000 & val64) <<  0) >> 54);
	*dest++ = (double)((int64_t)(( 0x003FF00000000000 & val64) << 10) >> 54);
	*dest++ = (double)((int64_t)(( 0x00000FFC00000000 & val64) << 20) >> 54);
	*dest++ = (double)((int64_t)(( 0x00000003FF000000 & val64) << 30) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000000000FFC000 & val64) << 40) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000000000003FF0 & val64) << 50) >> 54);
	rest64 =                               ( 0x000000000000000F & val64) << 60; // 4 bits rest.
	// 2nd: 64-bit word -> 6 samples and 8 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(((0xFC00000000000000 & val64) >> 4) | rest64) >> 54);
	*dest++ = (double)((int64_t)(( 0x03FF000000000000 & val64) <<  6) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000FFC000000000 & val64) << 16) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000003FF0000000 & val64) << 26) >> 54);
	*dest++ = (double)((int64_t)(( 0x000000000FFC0000 & val64) << 36) >> 54);
	*dest++ = (double)((int64_t)(( 0x000000000003FF00 & val64) << 46) >> 54);
	rest64 =                               ( 0x00000000000000FF & val64) << 56; // 8 bits rest.    
	// 3rd: 64-bit word -> 7 samples and 2 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(((0xC000000000000000 & val64) >> 8) | rest64) >> 54);
	*dest++ = (double)((int64_t)(( 0x3FF0000000000000 & val64) <<  2) >> 54);
	*dest++ = (double)((int64_t)(( 0x000FFC0000000000 & val64) << 12) >> 54);
	*dest++ = (double)((int64_t)(( 0x000003FF00000000 & val64) << 22) >> 54);
	*dest++ = (double)((int64_t)(( 0x00000000FFC00000 & val64) << 32) >> 54);
	*dest++ = (double)((int64_t)(( 0x00000000003FF000 & val64) << 42) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000000000000FFC & val64) << 52) >> 54);
	rest64 =                               ( 0x0000000000000003 & val64) << 62; // 2 bits rest.
	// 4th: 64-bit word -> 6 samples and 6 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(((0xFF00000000000000 & val64) >> 2) | rest64) >> 54);
	*dest++ = (double)((int64_t)(( 0x00FFC00000000000 & val64) <<  8) >> 54);
	*dest++ = (double)((int64_t)(( 0x00003FF000000000 & val64) << 18) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000000FFC000000 & val64) << 28) >> 54);
	*dest++ = (double)((int64_t)(( 0x0000000003FF0000 & val64) << 38) >> 54);
	*dest++ = (double)((int64_t)(( 0x000000000000FFC0 & val64) << 48) >> 54);
	rest64 =                               ( 0x000000000000003F & val64) << 58; // 6 bits rest.
	// 5th: 64-bit word -> 7 samples and no rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(((0xF000000000000000 & val64) >> 6) | rest64) >> 54);
	*dest++ = (double)((int64_t)(( 0x0FFC000000000000 & val64) <<  4) >> 54);
	*dest++ = (double)((int64_t)(( 0x0003FF0000000000 & val64) << 14) >> 54);
	*dest++ = (double)((int64_t)(( 0x000000FFC0000000 & val64) << 24) >> 54);
	*dest++ = (double)((int64_t)(( 0x000000003FF00000 & val64) << 34) >> 54);
	*dest++ = (double)((int64_t)(( 0x00000000000FFC00 & val64) << 44) >> 54);
	*dest++ = (double)((int64_t)(( 0x00000000000003FF & val64) << 54) >> 54);
      }
    } else if (nbits == 12) {
      while (iWord < nwords) {
	// 1st: 64-bit word -> 5 samples and 4 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(( 0xFFF0000000000000 & val64) <<  0) >> 52);
	*dest++ = (double)((int64_t)(( 0x000FFF0000000000 & val64) << 12) >> 52);
	*dest++ = (double)((int64_t)(( 0x000000FFF0000000 & val64) << 24) >> 52);
	*dest++ = (double)((int64_t)(( 0x000000000FFF0000 & val64) << 36) >> 52);
	*dest++ = (double)((int64_t)(( 0x000000000000FFF0 & val64) << 48) >> 52);
	rest64 =                               ( 0x000000000000000F & val64) << 60; // 4 bits rest.
	// 2nd: 64-bit word -> 5 samples and 8 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(((0xFF00000000000000 & val64) >> 4) | rest64) >> 52);
	*dest++ = (double)((int64_t)(( 0x00FFF00000000000 & val64) <<  8) >> 52);
	*dest++ = (double)((int64_t)(( 0x00000FFF00000000 & val64) << 20) >> 52);
	*dest++ = (double)((int64_t)(( 0x00000000FFF00000 & val64) << 32) >> 52);
	*dest++ = (double)((int64_t)(( 0x00000000000FFF00 & val64) << 44) >> 52);
	rest64 =                               ( 0x00000000000000FF & val64) << 56; // 8 bits rest.
	// 3rd: 64-bit word -> 6 samples and no rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (double)((int64_t)(((0xF000000000000000 & val64) >> 8) | rest64) >> 52);
	*dest++ = (double)((int64_t)(( 0x0FFF000000000000 & val64) <<  4) >> 52);
	*dest++ = (double)((int64_t)(( 0x0000FFF000000000 & val64) << 16) >> 52);
	*dest++ = (double)((int64_t)(( 0x0000000FFF000000 & val64) << 28) >> 52);
	*dest++ = (double)((int64_t)(( 0x0000000000FFF000 & val64) << 40) >> 52);
	*dest++ = (double)((int64_t)(( 0x0000000000000FFF & val64) << 52) >> 52);
      }
    } else {
      printf("ERROR: cannot handle %d bits per sample\n", nbits);
      return 0;
    }
  } else {
    for (iSample = 0; iSample < nsamples; iSample++) {
      double phi = 2.0*M_PI*(wave_phi/wave_length); //(double)iSample/1024.0;
      if (waveform == SINE_WAVE) {
	*dest = 1500.0*sin(phi);
      } else if (waveform == SQUARE_WAVE) {
	if (fmod(phi, 2.0*M_PI) < M_PI) {
	  *dest = -1500.0;
	} else {
	  *dest = +1500.0;
	}
      }
      if (round_flag) *dest = round(*dest);
      dest++;
      wave_phi += 1.0;
    }
  }
  iSample = 0;
  while ((*ndump > 0) && (iSample < nsamples)) {
    printf("# SAMPLE %d %f\n", idxSample, odest[iSample]);
    idxSample++;
    iSample++;
    (*ndump)--;
  }
  return 1;
}

static void do_pfb(int nbits, int nsamples, int ntaps, int nspectra, int ndump, int ftype, int fcprec, int waveform, double sample_freq, double wave_freq, FILE *data)
{
  uint64_t      *buffer;
  double       **taps;
  double        *window;
  fftw_complex  *input;
  fftw_complex  *result;
  double        *ps;
  size_t         nwords = nsamples*nbits/64;
  int            iTap, iSample;
  int            iSpectrum = 0;

  buffer  = (uint64_t*)calloc(nwords, sizeof(uint64_t));
  taps    = (double**)calloc(ntaps, sizeof(double*));
  if ((buffer == NULL) || (taps == NULL)) {
    fprintf(stderr, "cannot allocate memory for %d samples (buffer, taps)\n", nsamples);
    return;
  }
  for (iTap = 0; iTap < ntaps; iTap++) {
    taps[iTap] = (double*)calloc(nsamples, sizeof(double));
    if (taps[iTap] == NULL) {
      fprintf(stderr, "cannot allocate memory for taps[%d]\n", iTap);
      return;
    }
  }
  window  = (double*)calloc(nsamples*ntaps, sizeof(double));
  input   = (fftw_complex*)calloc(nsamples, sizeof(fftw_complex));
  result  = (fftw_complex*)calloc(nsamples, sizeof(fftw_complex));
  ps      = (double*)calloc(nsamples/2+1, sizeof(double));
  if ((window == NULL) || (input == NULL) || (result == NULL) || (ps == NULL)) {
    fprintf(stderr, "cannot allocate memory for %d samples (window, input, result, ps)\n", nsamples);
    return;
  }
  // Fensterfunktion: Flat-Top (Matlab)
  // a0	0.215578948
  // a1	0.416631580
  // a2	0.277263158
  // a3	0.083578947
  // a4	0.006947368
  double sumWindow = 0.0;
  switch (ftype) {
  case 0:
    for (iSample = 0; iSample < nsamples*ntaps; iSample++) {
      window[iSample] = 1.0;
    }
    sumWindow = (double)(nsamples*ntaps);
    break;
  case 1:
    for (iSample = 0; iSample < nsamples*ntaps; iSample++) {
      double x = M_PI*(double)(iSample)/(double)(nsamples*ntaps - 1);
      window[iSample] = 0.215578948 - 0.416631580*cos(2.0*x) + 0.277263158*cos(4.0*x) - 0.083578947*cos(6.0*x) + 0.006947368*cos(8.0*x);
      if (fcprec < 0) {
	window[iSample] = round(127*window[iSample]);
      } else if (fcprec > 0) {
	window[iSample] = mat_round_relative(window[iSample], fcprec);
      }
      printf("# WINDOW %d %f\n", iSample, window[iSample]);
      sumWindow += window[iSample];
    }
    default:;
    }
  double winScale = (double)nsamples/sumWindow;
  printf("# SETUP sumWindow %f\n", sumWindow);
  // Fuellen vor dem ersten Spektrum
  for (iTap = 0; iTap < ntaps-1; iTap++) {
    if (!read_samples_double(taps[iTap], buffer, nbits, nsamples, &ndump, waveform, sample_freq, wave_freq, data)) {
      fprintf(stderr, "not enough samples in file to do at least one spectrum\n");
      return;
    }
  }
  fftw_plan plan = fftw_plan_dft_1d(nsamples, input, result, FFTW_FORWARD, FFTW_ESTIMATE);
  while (read_samples_double(taps[ntaps-1], buffer, nbits, nsamples, &ndump, waveform, sample_freq, wave_freq, data)) {
    // Mit Fensterfunktion multiplizieren und aufaddieren
    for (iSample = 0; iSample < nsamples; iSample++) {
      input[iSample][0] = 0.0;
      input[iSample][1] = 0.0;
      for (iTap = 0; iTap < ntaps; iTap++) {
	input[iSample][0] += window[iTap*nsamples + iSample]*taps[iTap][iSample];
      }
    }
    if (iSpectrum == 0) {
      for (iSample = 0; iSample < nsamples; iSample++) {
	printf("# INPUT %d %g\n", iSample, input[iSample][0]);
      }
    }
    //printf("# DATA sumData %d %f\n", iSpectrum, sumData);
    //printf("# DATA sumSample %d %f\n", iSpectrum, sumSample);
    // powerspektrum berechnen und aufaddieren
    fftw_execute(plan);
    // update the power spectrum
    for (iSample = 0; iSample <= nsamples/2; iSample++) {
      ps[iSample] += (result[iSample][0]*result[iSample][0] + result[iSample][1]*result[iSample][1])/winScale/winScale;
    }
    iSpectrum++;
    if ((nspectra != -1) && (iSpectrum >= nspectra)) break;
    // Shift der Samples von einem Tap in den vorherigen (nur die Zeiger!)
    double *first = taps[0];
    for (iTap = 0; iTap < ntaps-1; iTap++) {
      taps[iTap] = taps[iTap+1];
    }
    taps[ntaps-1] = first;
  }
  if (iSpectrum != 0.0) {
    printf("# nbits %d nsamples %d ntaps %d nspectra %d\n", nbits, nsamples, ntaps, iSpectrum);
    for (iSample = 0; iSample <= nsamples/2; iSample++) {
      printf("# PFB %d %g\n", iSample, ps[iSample]/(double)iSpectrum/(double)nsamples);
    }
  }
  free(buffer);
  for (iTap = 0; iTap < ntaps; iTap++) {
    free(taps[iTap]);
  }
  free(window);
  free(input);
  free(result);
  free(ps);
}

int main(int argc, char *argv[])
{
  int     nbits = 8;
  int     nsamples = 4096;
  int     ntaps = 4;
  int     nspectra = -1;
  int     ndump = 0;
  int     ftype = 0;
  int     fcprec = 0;
  int     waveform = 0;
  double  sample_freq = 3200.0;
  double  wave_freq   = 3.1415926;
  int     iarg = 1;

  if (argc < 2) {
    printf("usage: %s { <options> } <file>\n", argv[0]);
    printf("  -nbits <nbits>\n");
    printf("              number of bits for each sample value [%d]\n", nbits);
    printf("  -nsamples <nsamples>\n");
    printf("              exponent of the number of samples (<nsamples> = 16 -> 65536 samples FFT) [%d]\n", 12);
    printf("  -ntaps <ntaps>\n");
    printf("              number of taps (VOLAC) [%d]\n", ntaps);
    printf("  -nspectra <nspectra>\n");
    printf("              number of spectra which are averaged [%d -> all spectra]\n", nspectra);
    printf("  -ftype <ftype>\n");
    printf("              filter window type (0 -> no window (FFT), 1 -> flat-top (PFB)) [%d]\n", ftype);
    printf("  -fcprec <fcprec>\n");
    printf("              precision of the filter coefficients (fcprec < 0 -> signed byte, fcprec == 0 -> double precision, fcprec > 0 -> number of significant digits) [%d]\n", fcprec);
    printf("  -data <waveform> [ <sample_freq> <wave_freq> ]\n");
    printf("              kind of data used (0 = samples, 1 = artificial sine wave, 2 = artificial square wave) [%d]\n", waveform);
    return 1;
  }
  while (iarg < argc-1) {
    if (strcmp(argv[iarg], "-nbits") == 0) {
      nbits = atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-nsamples") == 0) {
      nsamples = 1 << atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-ntaps") == 0) {
      ntaps = atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-nspectra") == 0) {
      nspectra = atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-ndump") == 0) {
      ndump = atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-ftype") == 0) {
      ftype = atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-fcprec") == 0) {
      fcprec = atoi(argv[++iarg]);
    } else if (strcmp(argv[iarg], "-data") == 0) {
      waveform = atoi(argv[++iarg]);
      if (waveform != 0) {
	sample_freq = atof(argv[++iarg]);
	wave_freq   = atof(argv[++iarg]);
      }
    } else {
      fprintf(stderr, "unknown option %s\n", argv[iarg]);
    }
    iarg++;
  }
  if (waveform == SAMPLES_WAVE) {
    FILE *data = fopen(argv[argc-1], "r");
    if (data != NULL) {
      do_pfb(nbits, nsamples, ntaps, nspectra, ndump, ftype, fcprec, waveform, sample_freq, wave_freq, data);
      fclose(data);
    }
  } else {
    do_pfb(nbits, nsamples, ntaps, nspectra, ndump, ftype, fcprec, waveform, sample_freq, wave_freq, NULL);
  }
  return 0;
}
