/*

  gcc -o recv_heaps recv_heaps.c -lpthread

*/


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <cstdint>
#include <unistd.h>
#include <endian.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <pthread.h>
#include <iostream>

#define MAX_THREADS           4
#define MAX_HEAPS          256
#define HEAP_HEADER_SIZE               sizeof(uint64_t)
#define HEAP_ITEM_SIZE                 sizeof(uint64_t)
#define MAX_HEAP_PAYLOAD_SIZE  16*1024*sizeof(uint8_t)

typedef struct {
  int                 isok;
  int                 nr;
  int                 sock; // socket connection for receiving heaps from one multicast group
  struct sockaddr_in  adr;
  struct ip_mreq      mreq;
  pthread_t           tid;
  size_t              available;
  size_t              used;
  uint8_t            *data;
  int                 heap_count;
  uint64_t            heap_id[MAX_HEAPS];
  uint64_t            heap_size[MAX_HEAPS];
  uint64_t            heap_received[MAX_HEAPS];
  uint64_t            boards[64];
  uint64_t            channels[4096];
} mcast_context_t;

static int             g_quiet       = 0;
static int             g_port        = 7148;
static int             g_infinite    = 0;
static int             g_spa_nskip   = 0;
static int             g_spa_npakets = 0;
static int             g_spa_nitems  = 0;
static mcast_context_t g_context;
static int             g_spa_count   = 0;
static uint64_t       *g_spa_items   = NULL;
static uint64_t       *g_spa_iptr    = NULL;

static int create_connection(mcast_context_t *context)
{
  int     rc;
  int     optval    = 1;
  u_char  loop_flag = 1;

  context->isok = 0;
  // create the socket, return 0 in case of error
  context->sock = socket(AF_INET, SOCK_DGRAM,0);
  if (context->sock < 0) {
    fprintf(stderr, "ERROR: can't create socket: %s\n",strerror(errno));
    return 0;
  }
  // bind to the local port
  rc = setsockopt(context->sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
  if (rc < 0) {
    fprintf(stderr, "ERROR: can't reuse socket address:%s\n", strerror(errno));
    return 0;
  }
  context->adr.sin_family      = AF_INET;
  context->adr.sin_addr.s_addr = htonl (INADDR_ANY);
  context->adr.sin_port        = htons(g_port);
  rc = bind(context->sock, (struct sockaddr *)&(context->adr), sizeof(context->adr));
  if (rc < 0) {
    fprintf(stderr, "ERROR: can't bind portnumber %d: %s\n", g_port, strerror(errno));
    return 0;
  }
  // activate multicast
  rc = setsockopt(context->sock, IPPROTO_IP, IP_MULTICAST_LOOP, &loop_flag, sizeof(loop_flag));
  if (rc < 0) {
    fprintf(stderr, "ERROR: cannot set IP_MULTICAST_LOOP: %s\n", strerror(errno));
    return 0;
  }
  context->available = HEAP_HEADER_SIZE + 15*HEAP_ITEM_SIZE + MAX_HEAP_PAYLOAD_SIZE;
  context->data = (uint8_t *)malloc(context->available);
  if (context->data == NULL) {
    fprintf(stderr, "ERROR: cannot allocate buffer for a SPEAD heap packet: %s\n", strerror(errno));
    return 0;
  }
  context->isok = 1;
  return 1;
}

static int add_connection(mcast_context_t *context, const char *interface_ip, const char *multicast_ip)
{
  int             rc;
  struct in_addr  ia;

  printf("add_connection(.., %s, %s)\n", interface_ip, multicast_ip);
  // join the multicast group:
  if (!inet_aton(multicast_ip, &ia)) {
    fprintf(stderr, "ERROR: cannot decode multicast address %s\n", multicast_ip);
    return 0;
  }
  context->mreq.imr_multiaddr.s_addr = ia.s_addr;
  if (!inet_aton(interface_ip, &ia)) {
    fprintf(stderr, "ERROR: cannot decode interface address %s\n", interface_ip);
    return 0;
  }
  context->mreq.imr_interface.s_addr = ia.s_addr;
  rc = setsockopt(context->sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &(context->mreq), sizeof(context->mreq));
  if (rc < 0) {
    fprintf(stderr, "ERROR: cannot add multicast membership: %s\n", strerror(errno));
    return 0;
  }
  return 1;
}

static int remove_connection(mcast_context_t *context, const char *interface_ip, const char *multicast_ip)
{
  int             rc;
  struct in_addr  ia;

  // leave the multicast group:
  if (!inet_aton(multicast_ip, &ia)) {
    fprintf(stderr, "ERROR: cannot decode multicast address %s\n", multicast_ip);
    return 0;
  }
  context->mreq.imr_multiaddr.s_addr = ia.s_addr;
  if (!inet_aton(interface_ip, &ia)) {
    fprintf(stderr, "ERROR: cannot decode interface address %s\n", interface_ip);
    return 0;
  }
  context->mreq.imr_interface.s_addr = ia.s_addr;
  rc = setsockopt(context->sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &(context->mreq), sizeof(context->mreq));
  if (rc < 0) {
    fprintf(stderr, "ERROR: cannot drop multicast membership: %s\n", strerror(errno));
    return 0;
  }
  return 1;
}

static int delete_connection(mcast_context_t *context)
{
  if (context->data != NULL) {
    free(context->data);
  }
  context->isok = 0;
  return 1;
}

static void process_heap(mcast_context_t *context)
{
  uint64_t   *spead_ptr = (uint64_t*)(context->data);
  uint64_t    spead_header;
  int         nitems, iitem;
  uint64_t    heap_id = 0;
  uint64_t    heap_size = 0;
  uint64_t    heap_received = 0;
  int         has_board = 0;
  uint64_t    board = 0;
  int         has_channel = 0;
  uint64_t    channel = 0;
  int         index = -1;

  spead_header = (uint64_t)(be64toh(spead_ptr[0]));
  nitems = (int)(spead_header & 0xffff);
  if (!g_quiet) printf("spead %016lx %d items", spead_header, nitems);
  for (iitem = 0; iitem < nitems; iitem++) {
    uint64_t   item;
    int        immediate_flag, item_tag;
    uint64_t   item_value;
    item = (uint64_t)(be64toh(spead_ptr[1 + iitem]));
    if ((g_spa_npakets != 0) && (g_spa_nskip == 0) && (g_spa_count != g_spa_npakets)) {
      *g_spa_iptr++ = item;
    }
    if (item & 0x8000000000000000) {
      immediate_flag = 1;
    } else {
      immediate_flag = 0;
    }
    item_tag = (int)((item & 0x7fff000000000000) >> 48);
    item_value = item & 0xffffffffffff;
    if (!g_quiet) printf(" %d %04x %012lx", immediate_flag, item_tag, item_value);
    switch (item_tag) {
    case 1: heap_id = item_value; break;
    case 2: heap_size = item_value; break;
    case 4: heap_received = item_value; break;
    //case 0x4101: board = item_value; has_board = 1; break;
    //case 0x4103: channel = item_value; has_channel = 1; break;
    default:;
    }
  }
  if (g_spa_npakets != 0) {
    if (g_spa_nskip > 0) {
      g_spa_nskip--;
    } else if (g_spa_count != g_spa_npakets) {
      g_spa_count++;
    }
    if (g_spa_count == g_spa_npakets) {
      int ipaket;
      g_spa_iptr = g_spa_items;
      FILE *dump = NULL;
      dump = fopen("spa.dat", "w");
      if (dump != NULL) {
        fwrite(g_spa_items, sizeof(std::uint64_t), g_spa_npakets*g_spa_nitems, dump);
	fclose(dump);
	dump = NULL;
      }
      for (ipaket = 0; ipaket < g_spa_npakets; ipaket++) {
        printf("p[%d] =", ipaket);
	for (iitem = 0; iitem < g_spa_nitems; iitem++) {
          uint64_t item = *g_spa_iptr++;
	  int        immediate_flag, item_tag;
	  uint64_t   item_value;
	  if (item & 0x8000000000000000) {
            immediate_flag = 1;
          } else {
            immediate_flag = 0;
          }
          item_tag = (int)((item & 0x7fff000000000000) >> 48);
          item_value = item & 0xffffffffffff;
	  switch (item_tag) {
          case 1: printf(" hid %012lx", item_value); break;
          case 2: /* printf(" hsz=%012lx", item_value); */ break;
          case 3: printf(" hof %012lx", item_value); break;
          case 4: /* printf(" psz=%012lx", item_value); */ break;
          case 0x1600: printf(" ts %012lx", item_value); break;
          case 0x1602: /* printf(" clp=%ld", item_value); */ break;
          case 0x1604: /* printf(" err=%012lx", item_value); */ break;
          case 0x1603: /* printf(" scl=%ld", item_value); */ break;
          case 0x1601:
            /* printf(" hwid %lx elid %ld chid %4ld", (item_value >> 32), item_value & 0xffff, (item_value >> 16) & 0xffff); */
            printf(" elid %ld chid %4ld", item_value & 0xffff, (item_value >> 16) & 0xffff);
            break;
	  case 0x1605: printf(" sn %ld", item_value); break;
          default:
          /* printf(" i[%d] = %d %04x %012lx", iitem, immediate_flag, item_tag, item_value); */
          ;
	  }
        }
	printf("\n");
      }
      g_spa_nskip   = 0;
      g_spa_npakets = 0;
      g_spa_nitems  = 0;
    }
  }
  for (index = context->heap_count - 1; index >= 0; index--) {
    if (context->heap_id[index] == heap_id) break;
    /*
    if (context->heap_id[index] < heap_id - 0x40) {
      index = -1;
      break;
    }
    */
  }
  if ((index == -1) && (context->heap_count < MAX_HEAPS)) {
    index = context->heap_count;
    context->heap_id[index] = heap_id;
    context->heap_size[index] = heap_size;
    context->heap_received[index] = 0;
    context->heap_count++;
    if (g_infinite) context->heap_count = context->heap_count % MAX_HEAPS;
  }
  context->heap_received[index] += heap_received;
  if (has_board) context->boards[board] = 1;
  if (has_channel) context->channels[channel] = 1;
  if (!g_quiet) printf("\n");
}

static void *receive_heaps(void *arg)
{
  mcast_context_t *context = (mcast_context_t *)arg;
  int              i;

  printf("receive_heaps(%d)\n", context->nr);
  context->heap_count = 0;
  for (i = 0; i < MAX_HEAPS; i++) {
    context->heap_id[i] = 0;
    context->heap_size[i] = 0;
    context->heap_received[i] = 0;
  }
  while (1) {
    uint8_t   *ptr = context->data;
    /* ssize_t br = */ recvfrom(context->sock, ptr, context->available, 0, NULL, NULL);
    //if (!g_quiet) printf("packet size=%ld\n", br);
    process_heap(context);
    if (context->heap_count == MAX_HEAPS) return NULL;
  }
}

static int parse_fixnum(std::int64_t &val, std::string &val_str)
{
  try {
    if (val_str.compare(0,2,"0x") == 0) {
      val = std::stol(std::string(val_str.begin() + 2, val_str.end()), nullptr, 16);
    } else if (val_str.compare(0,2,"0b") == 0) {
      val = std::stol(std::string(val_str.begin() + 2, val_str.end()), nullptr, 2);
    } else {
      val = std::stol(val_str, nullptr, 10);
    }
    return 1;
  } catch (std::exception& e) {
    return 0;
  }
}

static int add_multi_connection(mcast_context_t *context, const char *interface_ip, const char *multicast_ip)
{
  std::string val_str = multicast_ip;
  std::string::size_type str_from = 0, str_to, str_length = val_str.length();

  while (str_from < str_length + 1) {
    str_to = val_str.find_first_of(",", str_from);
    if (str_to == std::string::npos) str_to = str_length;
    if (str_to == str_from) break;
    std::string               el_str(val_str.data() + str_from, str_to - str_from);
    std::string::size_type    el_from = 0, el_to, el_length;
    std::int64_t              vals[8];
    std::int64_t              nadr = 0;
    std::int64_t              ipart;
    char                      pch = '.'; // prefix char: '.' -> address byte, '+' -> offset, '|' -> step, ':' -> port
    for (ipart = 0; ipart < 8; ipart++) vals[ipart] = 0;
    el_length = el_str.length();
    while ((el_from < el_length + 1)) {
      el_to = el_str.find_first_of(".+|:", el_from);
      if (el_to == std::string::npos) el_to = el_length;
      if (el_to == el_from) break;
      std::string hel(el_str.data() + el_from, el_to - el_from);
      // 0 .. 3  = address part
      // 4       = offset
      // 5       = step;
      // 7       = trash/unknown
      switch (pch) {
      case '.':
        if (nadr < 4) {
          ipart = nadr++;
        } else {
          ipart = 7; // -> trash
        }
        break;
      case '+':
        ipart = 4;
        break;
      case ':':
        ipart = 5;
        break;
      default:
        ipart = 7; // -> trash
      }
      if (!parse_fixnum(vals[ipart], hel)) {
        std::cerr << "Exception: cannot convert " << hel << " at position " << str_from << " into int for IP\n";
	return 0;
      }
      if (el_to < el_length) pch = el_str.at(el_to);
      el_from = el_to + 1;
    }
    str_from = str_to + 1;
    if (nadr != 4) {
      std::cerr << "Not a valid IPv4 address, less than 4 address bytes: " << el_str << '\n';
      continue;
    }
    if (vals[5] == 0) vals[5] = 1;
    if (!g_quiet) std::cout << "  IP sequence for " << vals[0] << "." << vals[1] << "." << vals[2] << "." << vals[3] << "+" << vals[4] << ":" << vals[5] << '\n';
    vals[4] += 1; // the n means up to l+n _including_
    while (vals[4] > 0) {
      char ip_adr[256];
      snprintf(ip_adr, sizeof(ip_adr), "%ld.%ld.%ld.%ld", vals[0], vals[1], vals[2], vals[3]);
      if (!add_connection(context, interface_ip, ip_adr)) return 0;
      vals[3] += vals[5];
      vals[4] -= vals[5];
    }
  }
  return 1;
}

static void usage(char *exename)
{
  printf("usage: %s [-quiet] [-port <port>] <interface_ip> { <multicast_ip> }\n", exename);
  printf("  -quiet         : only a summary after receiving %d heaps is printed\n", MAX_HEAPS);
  printf("  -port <port>   : changes the port from the default %d to a given number\n", g_port);
  printf("  -infinite      : receive heaps foreever\n");
  printf("  -spa <nskip> <npakets> <nitems> : sho the arrival of pakets by listing the item pointer values after receiving <npakets> pakets.\n");
  printf("  <interface_ip> : ip address (IPv4) of the network interface which will receive the data\n");
  printf("  <multicast_ip> : ip address (IPv4) of a UDP multicast group which send SPEAD heaps\n");
}

int main(int argc, char *argv[])
{
  int    i, j;
  int    isok     = 1;
  int    idx      = 1;
  int    if_index;
  int    mc_index;
  size_t tsize    = 0;
  size_t trec     = 0;

  if (argc <= 2) {
    usage(argv[0]);
    return 0;
  }
  do {
    if (strcmp(argv[idx], "-quiet") == 0) {
      g_quiet = 1;
      idx++;
      continue;
    } 
    if (strcmp(argv[idx], "-infinite") == 0) {
      g_infinite = 1;
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-port") == 0) {
      idx++;
      g_port = atoi(argv[idx++]);
      continue;
    }
    if (strcmp(argv[idx], "-spa") == 0) {
      idx++;
      g_spa_nskip   = atoi(argv[idx++]);
      g_spa_npakets = atoi(argv[idx++]);
      g_spa_nitems  = atoi(argv[idx++]);
      continue;
    }
    break;
  } while (1);
  for (i = 0; i < 64; i++) {
    g_context.boards[i] = 0;
  }
  for (i = 0; i < 4096; i++) {
    g_context.channels[i] = 0;
  }
  if_index = idx;
  mc_index = idx + 1;
  printf("create all connections\n");
  if (create_connection(&g_context)) {
    for (i = mc_index; i < argc; i++) {
      if (!add_multi_connection(&g_context, argv[if_index], argv[i])) {
	isok = 0;
	break;
      }
    }
  }
  if (isok && (g_spa_npakets != 0)) {
    g_spa_items = (uint64_t*)calloc(g_spa_npakets*g_spa_nitems, sizeof(uint64_t));
    g_spa_iptr = g_spa_items;
    if (g_spa_items == NULL) {
      isok = 0;
      printf("Could not allocate memory for %dx%d item pointers\n", g_spa_npakets, g_spa_nitems);
    }
  }
  if (isok) {
    // receiving and processing data from all open sockets
    printf("receiving and processing data\n");
    printf("receive and process SPEAD heaps comming from %d multicast group(s)\n", argc - mc_index);
    receive_heaps(&g_context);
  } else {
    // some error occured, do nothing
    printf("some error occured\n");
  }
  printf("closing all connections\n");
  for (i = mc_index; i < argc; i++) {
    if (!remove_connection(&g_context, argv[if_index], argv[i])) {
      isok = 0;
      break;
    }
  }
  delete_connection(&g_context);
  for (j = 1; j < g_context.heap_count - 1; j++) {
    tsize += g_context.heap_size[j];
    trec  += g_context.heap_received[j];
      printf("heap[%05d] %ld size %ld received %ld  = %.1f %%\n",
      j,
      g_context.heap_id[j],
      g_context.heap_size[j],
      g_context.heap_received[j],
      100.0*((double)(g_context.heap_received[j])/(double)(g_context.heap_size[j])));
  }
  printf("nheaps: %d total size %ld total received %ld average received %.1f\n",
	 g_context.heap_count-2,
	 tsize,
	 trec,
	 100.0*(double)trec/(double)tsize);
  for (i = 0; i < 64; i++) {
    if (g_context.boards[i] == 1) {
      printf("board %d\n", i);
    }
  }
  for (i = 0; i < 4096; i++) {
    if (g_context.channels[i] == 1) {
      printf("channel %d\n", i);
    }
  }
}


