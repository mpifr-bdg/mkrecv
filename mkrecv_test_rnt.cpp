// #define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>
#include <unistd.h>

#include "mkrecv_rnt.h"

#define NLOOPS 1000000
#define NFUNC  8

static std::shared_ptr<mkrecv::options>                   gOpts       = NULL;
static std::shared_ptr<spead2::thread_pool>               gThreadPool = NULL;
static std::shared_ptr<mkrecv::storage>                   gStorage    = NULL;
static std::vector<std::shared_ptr<mkrecv::allocator> >   gAllocators;

static int initTests(int argc, const char **argv)
{
  std::vector<int>  affinity;
  int               i;
  cpu_set_t         cpuset;

  // use the core list (from taskset) to fill the affinity vector
  sched_getaffinity(0, sizeof(cpuset), &cpuset);
  for (i = 0; i < CPU_SETSIZE; i++) {
    if (CPU_ISSET(i, &cpuset)) {
      std::cout << "CPU/CORE " << i << " used\n";
      affinity.push_back(i);
    }
  }
  // parse the command line options
  gOpts = std::shared_ptr<mkrecv::options>(new mkrecv::options());
  gOpts->parse_args(argc, argv);
  std::cout << gOpts->header << '\n';
  // create the thread pool using the core affinity
  gThreadPool.reset(new spead2::thread_pool(gOpts->nthreads, affinity));
  // Create a storage using static allocated slots (no connection to a real PSRDADA ringbuffer)
  try {
    gStorage = std::shared_ptr<mkrecv::storage>(new mkrecv::storage(gOpts));
    gStorage->open();
  } catch (std::runtime_error &e) {
    std::cerr << "Cannot connect to DADA Ringbuffer " << gOpts->dada_key << " exiting..." << '\n';
    exit(0);
  }
  // Create allocators for each input stream
  for (auto it = gOpts->sources.begin(); it != gOpts->sources.end(); ++it) {
    std::shared_ptr<mkrecv::allocator> allocator(new mkrecv::allocator(gOpts, gStorage));
    gAllocators.push_back(allocator);
  }
  return 0;
}

static void simulateStream()
{
  struct spead2::recv::packet_header  ph;
  // ph_pointers[0] = heap_cnt;
  // ph_pointers[1] = heap_length;
  // ph_pointers[2] = payload_offset;
  // ph_pointers[3] = payload_length;
  // ph_pointers[4] = timestamp;
  // ph_pointers[5] = polarization (bit 0);
  std::uint64_t  ph_pointers[8];
  std::uint64_t  ts_step;
  std::uint64_t  ts, pol;
  std::size_t               i, j;

  ts_step = gOpts->indices[0].step;
  ts      = ts_step;
  pol     = 0;
  ph.heap_address_bits = 48;
  ph.n_items = 2;
  ph.pointers = (std::uint8_t*)&(ph_pointers[4]);
  ph.heap_cnt       = 2*ts;
  ph.heap_length    = gOpts->heap_nbytes;
  ph.payload_offset = 0;
  ph.payload_length = gOpts->heap_nbytes;
  ph_pointers[0] = spead2::htobe((std::uint64_t)ph.heap_cnt);
  ph_pointers[1] = spead2::htobe((std::uint64_t)ph.heap_length);
  ph_pointers[2] = spead2::htobe((std::uint64_t)ph.payload_offset);
  ph_pointers[3] = spead2::htobe((std::uint64_t)ph.payload_length);
  for (i = 0; i < NLOOPS; i++) {
    for (j = 0; j < gAllocators.size(); j++) {
      ph.heap_cnt    = 2*ts;
      ph_pointers[0] = spead2::htobe((std::uint64_t)ph.heap_cnt);
      ph_pointers[4] = spead2::htobe(ts);
      ph_pointers[5] = spead2::htobe(pol);
      gAllocators.at(j)->allocate(gOpts->heap_nbytes, &ph);
      gAllocators.at(j)->mark(ph.heap_cnt, true, gOpts->heap_nbytes);
      pol = (pol + 1)%2;
      if (pol == 0) ts += ts_step;
    }
    usleep(2);
  }
#ifdef ENABLE_TIMING_MEASUREMENTS
  mkrecv::timing_statistics  et;
  for (j = 0; j < gAllocators.size(); j++) {
    et.add_et(gAllocators.at(j)->et);
  }
  std::cout << "allocator: ";
  et.show();
#endif
}


int main(int argc, const char **argv)
{
  initTests(argc, argv);

  std::chrono::high_resolution_clock::time_point hmt1 = std::chrono::high_resolution_clock::now();
  simulateStream();
  std::chrono::high_resolution_clock::time_point hmt2 = std::chrono::high_resolution_clock::now();

  std::chrono::high_resolution_clock::time_point dmt1 = std::chrono::high_resolution_clock::now();
  // ...
  std::chrono::high_resolution_clock::time_point dmt2 = std::chrono::high_resolution_clock::now();

  std::cout << "total time: loops " << NLOOPS
	    << " hash map "
	    << std::chrono::duration_cast<std::chrono::nanoseconds>( hmt2 - hmt1 ).count()
	    << " fc "
	    << (double)(std::chrono::duration_cast<std::chrono::nanoseconds>( hmt2 - hmt1 ).count())/((double)NFUNC*NLOOPS)
	    << " direct map "
	    << std::chrono::duration_cast<std::chrono::nanoseconds>( dmt2 - dmt1 ).count()
	    << " fc "
	    << (double)(std::chrono::duration_cast<std::chrono::nanoseconds>( dmt2 - dmt1 ).count())/((double)NFUNC*NLOOPS)
	    << " ns\n";

  return 0;
}

/*
./mkrecv_test --nindices 3 --idx1-item 0 --idx1-step 4096 --idx2-item 1 --idx2-list 0,1,2,3 --idx3-item 3 --idx3-list 41,43,45,57

./mkrecv_test --nindices 3 --idx1-item 0 --idx1-step 4096 --idx2-item 1 --idx2-list 0,1,2,3 --idx3-item 3 --idx3-list 164,168,172,228

./mkrecv_test --nindices 3 --idx1-item 0 --idx1-step 4096 --idx2-item 1 --idx2-list 0,1,2,3 --idx3-item 3 --idx3-list 0,8,16,24,32,40,48,56

 */
