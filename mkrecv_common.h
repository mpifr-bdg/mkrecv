#ifndef mkrecv_common_h
#define mkrecv_common_h

//#define ENABLE_TIMING_MEASUREMENTS    1
//#define ENABLE_CORE_MUTEX             1
#define USE_STD_MUTEX                 1

#define USE_SCALED_TIMESTAMP        1
#define HANDLE_TIMESTAMP_ROLLOVER   1
#define TS_IS_48_BITS               1
//#define TS_IS_44_BITS               1
#define USE_PSRDADA_SLOTS           1
#define USE_FREERUN_SWITCH          1

#define REPORT_MISSING_HEAPS        1

//#define COMBINED_IP_PORT            1

#define SCI_EMPTY -1

namespace mkrecv {

  template<class T>
    void inc(T &value, const T max_value)
    {
      if (value == max_value)
        {
          value = 0;
        }
      else
        {
          value++;
        }
    }

  template<class T>
    void dec(T &value, const T max_value)
    {
      if (value == 0)
        {
          value = max_value;
        }
      else
        {
          value--;
        };
    }

}

#endif /* mkrecv_common_h */

